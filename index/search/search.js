let rwd = 0;
let hd = "";
let cooKies = function (cookies) {
  let list = wx.getStorageSync("searchList") || []
  list.unshift(cookies)
  wx.setStorageSync("searchList", list)
}
Page({
  data: {
    wd: 0,
    rwd: 0,
    hd: "",
    key: 'searchList',
    saveData: "",
    searchList: []
  },
  onLoad: function (options) {
    // 获取设备宽度
    let wd = wx.getSystemInfoSync().windowWidth,
      rwd = wd - 100
    hd = 'width:' + rwd + 'px;'
    this.setData({
      hd: hd
    })
  },
  // 清除搜索历史
  deleteHistory(e) {
    wx.clearStorageSync('searchList')
    this.onShow()
  },
  // 搜索框变动
  inputChanged(e) {
    console.log(e.detail)
    this.setData({
      saveData: e.detail
    })
  },
  // 搜索事件
  submit(e) {
    cooKies(this.data.saveData)
    let list = wx.getStorageSync("searchList") || []
    this.setData({
      searchList: list
    })
  },
  onShow(e){
    let list = wx.getStorageSync("searchList") || []
    this.setData({
      searchList: list
    })
  }
})