// index/shopping/shopping.js
let rwd = 0;
let hd = ""
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carts: [{
      id: 1,
      business: '西伯利亚',
      title: '西伯利亚草莓果',
      label: '即使青春是一株大地伟岸的树，但我明白，一株独秀永远不是挺拔，成行成排的林木，才是遮风挡沙的绿色长城。1',
      image: '/images/sells/Agricultural_products1.png',
      nuber_value: 1,
      price: 33.00,
    }, {
      id: 2,
      title: '奥地利奶油蛋糕',
      label: '即使青春是一株大地伟岸的树，但我明白，一株独秀永远不是挺拔，成行成排的林木，才是遮风挡沙的绿色长城。2',
      business: '奥地利',
      image: '/images/sells/Agricultural_products2.png',
      nuber_value: 1,
      price: 88.00,
    }, {
      id: 2,
      title: '奥地利大奶油蛋糕',
      label: '即使青春是一株大地伟岸的树，但我明白，一株独秀永远不是挺拔，成行成排的林木，才是遮风挡沙的绿色长城。3',
      business: '奥地利',
      image: '/images/sells/Agricultural_products3.png',
      nuber_value: 1,
      price: 78.00,
    }, {
      id: 2,
      title: '圣地亚哥小白菜',
      label: '即使青春是一株大地伟岸的树，但我明白，一株独秀永远不是挺拔，成行成排的林木，才是遮风挡沙的绿色',
      business: '圣地亚哥',
      image: '/images/sells/Agricultural_products.png',
      nuber_value: 98,
      price: 11.00,
    }],
    nuber_value: 1,
    totalPrice: 0,
    info: "",
    // 获取设备宽度
    wd: 0,
    rwd: 0,
    hd: ""
  },

  // 商品减少
  reduce: function (e) {
    const index = e.currentTarget.dataset.index;
    let carts = this.data.carts;
    let nuber_value = carts[index].nuber_value;
    if (nuber_value <= 1) {
      wx.showToast({
        title: '已经最小咯~',
        icon: 'none'
      })
      return false;
    }
    nuber_value = nuber_value - 1;
    carts[index].nuber_value = nuber_value;
    this.setData({
      carts: carts
    });
    this.getTotalPrice();
  },
  // 商品增加
  increase: function (e) {
    const index = e.currentTarget.dataset.index;
    let carts = this.data.carts;
    let nuber_value = carts[index].nuber_value;
    nuber_value = nuber_value + 1;
    carts[index].nuber_value = nuber_value;
    this.setData({
      carts: carts
    });
    this.getTotalPrice()
  },
  // 总价合计
  getTotalPrice() {
    let carts = this.data.carts; // 获取购物车列表
    let total = 0;
    for (let i = 0; i < carts.length; i++) { // 循环列表得到每个数据
      total += (carts[i].nuber_value * carts[i].price); // 所有价格加起来
    }
    this.setData({ // 最后赋值到data中渲染到页面
      carts: carts,
      totalPrice: total.toFixed(2)
    });
    console.log(total.toFixed(2));
  },
  // 删除商品
  deleteList(e) {
    console.log(this.data.carts.length);
    console.log(e);
    const index = e.currentTarget.dataset.index;
    let carts = this.data.carts;
    carts.splice(index, 1); // 删除购物车列表里这个商品
    this.setData({
      carts: carts
    });
    this.getTotalPrice(); // 重新计算总价格
  },
  onShow() {

  },
  // 进入获取购物车总值
  onLoad: function () {
    var that = this;
    that.getTotalPrice();
    // 获取设备宽度
    let wd = wx.getSystemInfoSync().windowWidth,
      rwd = wd-100
    hd = 'left:' + rwd + 'px;'
    this.setData({
      hd: hd
    })
    console.log(hd)
  }
  // ,
  // onShow: function () {
  //   console.log(this.data.carts.length);
  //   if (this.data.carts.length <= 2) {
  //     this.setData({
  //       info: "购物车空啦"
  //     })
  //   };
  // }
})