// index/me/me.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  // 购买订单跳转
  myorder() {
   wx.navigateTo({
     url: '../me/myorder/myorder?id=2',
   })
  },
  // 物流跳转
  mylogistics() {
    wx.navigateTo({
      url: '../me/logistics/logistics',
    })
  },
  // 购买历史
  bhistory(){
    wx.navigateTo({
      url: '../me/bhistory/bhistory',
    })
  },
  // 售卖历史
  shistory(){
    wx.navigateTo({
      url: '../me/shistory/shistory',
    })
  },
  // 浏览历史
  lhistory(){
    wx.navigateTo({
      url: '../me/lhistory/lhistory',
    })
    console.log(getCurrentPages())

  },
  shopAT() {
    wx.navigateTo({
      url: '../me/bargin/bargin',
    })
  },
  productAT() {
    wx.navigateTo({
      url: '../me/discount/discount',
    })
  },
  mybought() {
    wx.navigateTo({
      url: '../me/bought/bought',
    })
  },
  storecommodity() {
    wx.navigateTo({
      url: '../me/sell/sell',
    })
  },
  // 列表展览
  data() {
    return {
      activeName: '1'
    };
  },
  onChange(event) {
    this.setData({
      activeName: event.detail
    });
  },
  signClick() {
    wx.navigateTo({
      url: '../yunlogin/yunlogin',
    })
  },
  upClick(){
wx.navigateTo({
  url: './sell/sell',
})
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})