// index/me/myorder/myorder.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value: '',
    activeKey: 0,
    steps: [
      {
        text: '买家下单',
        desc: '描述信息'
      },
      {
        text: '商家接单',
        desc: '描述信息'
      },
      {
        text: '买家签收',
        desc: '描述信息'
      },
      {
        text: '交易完成',
        desc: '描述信息'
      }
    ]
  },
  logistics(){
    wx.navigateTo({
      url: '../../logistics/logistics?id=1',
    })
  },
  onClickLeft() {
   wx.navigateBack()
  },
  onChange(event) {
    wx.showToast({
      icon: 'none',
      title: '切换至第${event.detail}项'
    });
  },
  onSearch() {
    Toast('搜索' + this.data.value);
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})