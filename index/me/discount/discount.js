// index/me/discount/discount.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperCurrent: 0,
    imgUrls: ['https://p3.pstatp.com/large/43700001e49d85d3ab52', 'https://p3.pstatp.com/large/39f600038907bf3b9c96', 'https://p3.pstatp.com/large/31fa0003ed7228adf421']
  },
  //轮播图的切换事件 
  swiperChange: function (e) {
    this.setData({ swiperCurrent: e.detail.current })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})