// index/sell/sell.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '李德',
    idNo: '430722196908224236',
    fileList: [],
    files: [{
      url: 'http://mmbiz.qpic.cn/mmbiz_png/VUIF3v9blLsicfV8ysC76e9fZzWgy8YJ2bQO58p43Lib8ncGXmuyibLY7O3hia8sWv25KCibQb7MbJW3Q7xibNzfRN7A/0',
  }, {
      loading: true
  }, {
      error: true
  }]
  },
  beforeRead(event) {
    const {
      file
    } = event.detail;
    callback(file.type === 'image');
    wx.uploadFile({
      filePath: file.path,
      name: 'file',
      formData: {
        user: 'test'
      },
      success(res) {
        // 上传完成需要更新 fileList
        const {
          fileList = []
        } = this.data;
        fileList.push({
          ...file,
          url: res.data
        });
        this.setData({
          fileList
        });
      }
    });
    wx.showToast({
        title: '加载中',
        icon: 'loading',
        duration: 10000
      }),
      setTimeout(function () {
        wx.hideToast()
      }, 100)
  },
  afterRead(e) {

  },
  onClickLeft(e) {
    wx.navigateBack({

    })
  },
  inouvccode(e) {

    let re = 'http://ue9dyr.natappfree.cc/currency/api/admin/sys/login/code.do'
    wx.request({
      url: re,
    })
    console.log(e)
    this.setData({
      inouvccodeurl: re
    });
  },
  // 图片上传
  onLoad() {
    this.setData({
        selectFile: this.selectFile.bind(this),
        uplaodFile: this.uplaodFile.bind(this)
    })
},
  chooseImage: function (e) {
    var that = this;
    wx.chooseImage({
        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
            // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
            that.setData({
                files: that.data.files.concat(res.tempFilePaths)
            });
        }
    })
},
  previewImage: function(e){
    wx.previewImage({
        current: e.currentTarget.id, // 当前显示图片的http链接
        urls: this.data.files // 需要预览的图片http链接列表
    })
},
selectFile(files) {
    console.log('files', files)
    // 返回false可以阻止某次文件上传
},
uplaodFile(files) {
    console.log('upload files', files)
    // 文件上传的函数，返回一个promise
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject('some error')
        }, 1000)
    })
},
uploadError(e) {
    console.log('upload error', e.detail)
},
uploadSuccess(e) {
    console.log('upload success', e.detail)
}
  // testone: function() {
  //   wx: wx.navigateTo({
  //     url: '../yunlogin/yunlogin',
  //     success: function(res) {
  //       wx.showToast({
  //         title: '加载中',
  //         icon: 'loading',
  //         duration: 300
  //       })

  //       setTimeout(function() {
  //         wx.hideToast()
  //       }, 100)
  //     },
  //   }),

  //   wx.chooseImage({
  //     count: 1, // 默认9
  //     sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
  //     sourceType: ['camera', 'album'], // 可以指定来源是相册还是相机，默认二者都有
  //     success: function(res) {
  //       // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
  //       var tempFilePaths = res.tempFilePaths
  //       that.setData({
  //         sell_imgs: res.tempFilePaths
  //       })
  //     }
  //   })
  // },
  // sellimg() {
  //   let that = this
  //   wx.chooseImage({
  //     count: 1, // 默认9
  //     sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
  //     sourceType: ['camera', 'album'], // 可以指定来源是相册还是相机，默认二者都有
  //     success: function(res) {
  //       // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
  //       var tempFilePaths = res.tempFilePaths
  //       that.setData({
  //         sell_imgs: res.tempFilePaths
  //       })
  //     }
  //   })
  // }


})