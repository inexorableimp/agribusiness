const app = getApp();
Page({
  data: {
    // 判断小程序的API，回调，参数，组件等是否在当前版本可用。
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },
  onLoad: function () {
    var that = this;
    // 查看是否授权
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            success: function (res) {
              // 从数据库获取用户信息
              that.queryUserInfo();
            }
          });
        }
      }
    })
  },
  bindGetUserInfo: function (e) {
    //用户点了同意
    if (e.detail.userInfo) {
      //获取用户openid
        // wx.login({
        //   success: function (res) {
        //     if (res.code) {
        //       // 获取openId
        //       wx.request({
        //         url: 'https://api.weixin.qq.com/sns/jscode2session',
        //         data: {
        //           // 小程序唯一标识
        //           appid: 'wx462f08b2b136bdf5',
        //           // 小程序的 app secret
        //           secret: '',
        //           grant_type: 'authorization_code',
        //           js_code: res.code
        //         },
        //         method: 'GET',
        //         header: { 'content-type': 'application/json' },
        //         success: function (openIdRes) {
        //           console.info("登录成功返回的openId：" + openIdRes.data.openid);
        //           app.globalData.openId = openIdRes.data.openid;
        //           // 判断openId是否获取成功
        //           if (openIdRes.data.openid != null & openIdRes.data.openid != undefined) {
        //             // 拿到用户信息
        //             wx.getUserInfo({
        //               success: function (data) {
        //                 // 自定义操作
        //                 // 绑定数据，渲染页面
        //                 that.setData({

        //                 });
        //               },
        //               fail: function (failData) {
        //                 console.info("用户拒绝授权");
        //               }
        //             });
        //           } else {
        //             console.info("获取用户openId失败");
        //           }
        //         },
        //         fail: function (error) {
        //           console.info("获取用户openId失败");
        //           console.info(error);
        //         }
        //       })
        //     }
        //   }
        // });
      // 用户按了允许授权按钮
      wx.switchTab({
        url: '/index/home/home'
      })
      //  var that = this;
      // 将授权通过的用户相关信息保存到数据库
      // wx.request({
      //   url: app.globalData.urlPath + 'user/add',
      //   data: {
      //     openid: getApp().globalData.openid,
      //     nickName: e.detail.userInfo.nickName,
      //     avatarUrl: e.detail.userInfo.avatarUrl,
      //     province: e.detail.userInfo.province,
      //     city: e.detail.userInfo.city
      //   },
      //   header: {
      //     'content-type': 'application/json'
      //   },
      //   success: function (res) {
      //     // 从数据库获取用户信息
      //     that.queryUsreInfo();
      //     console.log("插入小程序登录用户信息成功！");
      //   }
      // });
    } else {
      // 用户按了拒绝按钮
      wx.showModal({
        title: '警告',
        content: '您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!',
        showCancel: false,
        confirmText: '返回授权',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击了“返回授权”')
          }
        }
      })
    }
  },
  // 获取用户信息接口
  queryUserInfo: function () {
    wx.request({
      url: app.globalData.urlPath + 'user/userInfo',
      method: 'post',
      header: { "Content-Type": "application/x-www-form-urlencoded" },
      data: {
        openid: app.globalData.openid
      },
      success: function (res) {
        if(res.code == 200){
          getApp().globalData.userInfo = res.userInfo;
          wx.switchTab({
            url: '/index/home/home'
          })
        }else{
          //openid错误
          wx.showModal({
            title: '警告',
            content: '您未同意授权，将无法进入小程序，请返回授权之后再进入!!!',
            showCancel: false,
            confirmText: '返回授权',
            success: function (res) {
              if (res.confirm) {
                console.log('返回授权')
                wx.redirectTo({
                  url: 'pages/index/authorization/isAuthorization'
                })
              }
            }
          })
        }
      }
    });
  },

})
 