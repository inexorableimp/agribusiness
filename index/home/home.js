// index/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 搜索区
    searcharea: "请输入您搜索的商品",

    // 轮播图
    currentIndex: 0,
    imgUrls: ['https://p3.pstatp.com/large/43700001e49d85d3ab52', 'https://p3.pstatp.com/large/39f600038907bf3b9c96', 'https://p3.pstatp.com/large/31fa0003ed7228adf421'],
    links: ['../user/user', '../user/user', '../user/user'],


    // 分类导航栏

    activetwo: 0,
    btns: ["特价区", "推荐", "上新", "抢鲜", "分类五"],


    // 商品展览
    item_image: [{
        name: '起名太难了1',
        tag1: '甜品1',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '43'
      },
      {
        name: '起名太难了2',
        tag1: '甜品2',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products1.png',
        price: '41'
      },
      {
        name: '起名太难了3',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '23'
      },
      {
        name: '起名太难了4',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products3.png',
        price: '26'
      },
      {
        name: '起名太难了5',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products3.png',
        price: '25'
      },
      {
        name: '起名太难了6',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '23'
      },
      {
        name: '起名太难了7',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products.png',
        price: '34'
      },
      {
        name: '起名太难了8',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products1.png',
        price: '23'
      },
      {
        name: '起名太难了9',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '32'
      }, {
        name: '起名太难了10',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products3.png',
        price: '54'
      },
      {
        name: '起名太难了11',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products1.png',
        price: '34'
      }, {
        name: '起名太难了12',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '65'
      }, {
        name: '起名太难了13',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products3.png',
        price: '75'
      }, {
        name: '起名太难了14',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '67'
      }, {
        name: '起名太难了15',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products1.png',
        price: '56'
      }, {
        name: '起名太难了16',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '67'
      }, {
        name: '起名太难了17',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products3.png',
        price: '23'
      },
      {
        name: '起名太难了18',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '43'
      }, {
        name: '起名太难了25',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '32'
      }, {
        name: '起名太难了19',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '43'
      }, {
        name: '起名太难了20',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '32'
      }, {
        name: '起名太难了21',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '43'
      }, {
        name: '起名太难了22',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '32'
      }, {
        name: '起名太难了23',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '43'
      }, {
        name: '起名太难了24',
        tag1: '甜品',
        tag2: '零食',
        tag3: '糕点',
        img: '/images/sells/Agricultural_products2.png',
        price: '32'
      },
    ],
    // 底部商品
    foot_img: []
  },
  // 搜索栏跳转
  searchTo: function () {
    wx.navigateTo({
      url: '/index/search/search',
    })
  },
  // 轮播图动画切换
  swiperChange: function (e) {
    this.setData({
      currentIndex: e.detail.current
    })

  },
  // 点击指示点切换 
  chuangEvent: function (e) {
    this.setData({
      swiperCurrent: e.currentTarget.id
    })
  },
  //点击图片触发事件  
  swipclick: function (e) {
    console.log(this.data.swiperCurrent);
    wx.navigateTo({
      url: '../details/details',
    })
    wx.switchTab({
      url: this.data.links[this.data.swiperCurrent]
    })
  },


  // 分类导航栏
  changeTab: function (e) {
    this.setData({
      activetwo: e.currentTarget.dataset.index,
    })
  },

  // 获取本地图片路径
  getsellList: function () {
    let that = this;
    wx.request({
      url: 'http://ww.hengyishun.cn/login/navilist',
      success(res) {
        that.setData({
          sellList: data
        })
      }
    })
  },
  imgclick() {
    wx.navigateTo({
      url: '../details/details'
    })
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 商品展览
    var that = this;

    /**
     * 获取当前设备的宽高
     */
    wx.getSystemInfo({

      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }

    });


    var prevExitState = this.exitState // 尝试获得上一次退出前 onSaveExitState 保存的数据
    if (prevExitState !== undefined) { // 如果是根据 restartStrategy 配置进行的冷启动，就可以获取到
      prevExitState.myDataField === 'myData'
    }
  },
  /**
   * 生命周期函数--监听页面保存时间
   */

  onSaveExitState: function () {
    var exitState = {
      myDataField: 'myData'
    } // 需要保存的数据
    return {
      data: exitState,
      expireTimeStamp: Date.now() + 24 * 60 * 60 * 1000 // 超时时刻
    }
  }
})