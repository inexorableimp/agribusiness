// index/details/details.js

Page({

  /**
   * 页面的初始数据
   * 
   * container: null,
   */
  data: {
    tags:['标签一dsadsa','标签二dasdas','标签三asdas','标签一','标签二','标签三'],
    exhibition:['https://img.yzcdn.cn/vant/cat.jpeg','https://img.yzcdn.cn/vant/cat.jpeg','https://img.yzcdn.cn/vant/cat.jpeg','https://img.yzcdn.cn/vant/cat.jpeg','https://img.yzcdn.cn/vant/cat.jpeg'],
    swiperCurrent: 0,
    imgalist: ['https://p3.pstatp.com/large/43700001e49d85d3ab52', 'https://p3.pstatp.com/large/39f600038907bf3b9c96','https://p3.pstatp.com/large/31fa0003ed7228adf421'],
    "tab":['产品标签','产品展览','规格说明']
  },
  // 轮播图点击预览事件
  previewImage: function (e) {
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: this.data.imgalist // 需要预览的图片http链接列表
    })
  },
  //轮播图的切换事件 
  swiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
    })
  },
  // 评分数据
  onChange(event) {
    this.setData({
      value: event.detail
    });
  },
  onClickLeft() {
    wx.navigateBack()
  },
  shoppingClick(e){
    wx.switchTab({
      url: '/index/shopping/shopping',
    })
  }
})  